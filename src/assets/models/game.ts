/* This represents an instance of a game. */
import {LetterMap} from './letterMap';
import {LetterChooser} from './letterChooser';
import {GenericBlock} from './blocks/genericBlock';
import {QuotationBlock} from './blocks/quotationBlock';
import {ReflectionalBlock} from './blocks/ReflectionalBlock';
import {RotationalBlock} from './blocks/RotationalBlock';

export class Game {
    private readonly appVersion: number = 1; // Increment this every time we make a material change to the app.
    private ref: number = new Date().getTime();
    public letterMap: LetterMap;
    public blocks: GenericBlock[] = [];

    constructor(private name?: string) {
        this.name = this.name === null ? 'New Game' : this.name;
        this.letterMap = new LetterMap();
        // Add an initial block.
        this.addReflectionalBlock(
            ReflectionalBlock.DEFAULT_BLOCK_SIZE,
            ReflectionalBlock.DEFAULT_BLOCK_SIZE,
            true
        );
    }

    /**
     * This method receives a generic object that represents a game state (eg from local storage), and updates
     * the state of this game to match it.
     * @param gameState
     */
    public setState(gameState) {
        // The very first thing we do is upgrade the game data in case it was saved by a previous version of the
        // application. It makes the stored data compatible with the current version of the game class.
        this.upgradeToLatestVersion(gameState);

        // Now "copy over" the data from the stored object into this game.
        this.ref = gameState.ref;
        this.name = gameState.name;
        this.letterMap.setState(gameState.letterMap);
        this.blocks = [];
        for (const block of gameState.blocks) {
            let newBlock: GenericBlock = null;
            switch (block.hasOwnProperty('blockType') ? block.blockType : 'unknown') {
                case 'reflectional':
                    newBlock = new ReflectionalBlock(block.blockWidth, block.blockHeight, block.isSquare);
                    break;
                case 'rotational':
                    newBlock = new RotationalBlock(block.blockWidth, block.blockHeight, block.isSquare);
                    break;
                case 'quotation':
                    newBlock = new QuotationBlock(block.blockWidth, block.blockHeight);
                    break;
                default:
                    alert('Error loading saved game (blocks)');
                    break;
            }
            newBlock.setRows(block.rows);
            this.blocks.push(newBlock);
        }
    }

    /**
     * Manage any necessary upgrade. Suppose the current app version is 9. Basically, if the version in the stored
     * game is lower than 9 (the current app version), we call "upgrade8to9". The first thing that does is check
     * whether the stored version is lower than 8. If it is, it calls "upgrade7to8". That checks the version
     * against 7 and calls "upgrade6to7" as necessary. This chain goes all the way to "upgrade0to1" if necessary.
     */
    private upgradeToLatestVersion(gameState) {
        if (this.versionIsLowerThan(gameState, 1)) {
            this.upgrade0to1(gameState); // This must always call "previous version" to "current version".
        }
    }

    private versionIsLowerThan(gameState, version) {
        // If there's no version property, it's deemed to be version 0.
        const actualVersion = gameState.hasOwnProperty('appVersion') ? gameState.appVersion : 0;

        return actualVersion < version;
    }

    private upgrade0to1(gameState) {
        // This upgrade does the following:
        //  add the version number property to the game state.
        //  Go through all its blocks and converts any "blockSize"s to "blockWidth" & "blockHeight".
        gameState.appVersion = 1;
        if (gameState.hasOwnProperty('blocks')) {
            gameState.blocks.forEach(
                function (block) {
                    if (block.hasOwnProperty('blockType')) {
                        // At this version, all reflectional/rotational blocks were square.
                        block.isSquare = block.blockType !== 'quotation';
                    }
                    if (block.hasOwnProperty('blockSize')) {
                        block.blockWidth = block.blockSize;
                        block.blockHeight = block.blockSize;
                        delete block.blockSize;
                    }
                }
            );
        }

    }

    public getRef(): number {
        return this.ref;
    }

    public getName(): string {
        return this.name;
    }

    public isLetterUsed(thisLetter: string): boolean {
        return this.letterMap.isLetterUsed(thisLetter);
    }

    public getMappedLetter(fromNumber: any): string {
        // If the code is not numeric, just return it as is.
        if (isNaN(fromNumber)) {
            return fromNumber;
        }
        // Otherwise, see if there's a mapping.
        const mappedLetter = this.letterMap.getMapping(fromNumber);
        if (mappedLetter == null) {
            return null;
        }

        return mappedLetter.getLetter();
    }

    public getLetterCertainty(fromNumber: number): number {
        const mappedLetter = this.letterMap.getMapping(fromNumber);
        if (mappedLetter == null) {
            return 0;
        }

        return mappedLetter.getCertainty();
    }

    public setMapping(chooser: LetterChooser) {
        this.letterMap.setMapping(chooser);
    }

    public isCodeHighlighted(codeNumber: number): boolean {
        return this.letterMap.isCodeHighlighted(codeNumber);
    }

    public isComplete(): boolean {
        return this.letterMap.allAreCertain();
    }

    public addRotationalBlock(blockWidth: number, blockHeight: number, isSquare: boolean) {
        this.blocks.push(new RotationalBlock(blockWidth, blockHeight, isSquare));
    }

    public addReflectionalBlock(blockWidth: number, blockHeight: number, isSquare: boolean) {
        this.blocks.push(new ReflectionalBlock(blockWidth, blockHeight, isSquare));
    }

    public addQuotationBlock(blockWidth: number, blockHeight: number) {
        this.blocks.push(new QuotationBlock(blockWidth, blockHeight));
    }

    public deleteBlock(blockIdx: number) {
        // Remove the given block from the array of blocks.
        this.blocks.splice(blockIdx, 1);
    }

    public moveBlockBy(blockIdx: number, by: number) {
        // Move the given block "up" by the given amount within the array of blocks.
        // Of course. "by" can be negative.
        const ourBlock = this.blocks.splice(blockIdx, 1);
        this.blocks.splice(blockIdx + by, 0, ourBlock.shift());
    }

    public addColumn(blockIdx: number) {
        this.blocks[blockIdx].addColumn();
    }

    public removeColumn(blockIdx: number) {
        this.blocks[blockIdx].removeColumn();
    }

    public addRow(blockIdx: number) {
        this.blocks[blockIdx].addRow();
    }

    public removeRow(blockIdx: number) {
        this.blocks[blockIdx].removeRow();
    }

    public toggleSquareness(blockIdx: number) {
        this.blocks[blockIdx].toggleSquareness();
    }

    public setName(newName: string) {
        this.name = newName;
    }

    public resetGame() {
        this.letterMap.clearAllButGiven();
    }

    public setCellHighlight(cellCode: number, isOn: boolean) {
        // Highlight the cells in the blocks.
        this.blocks.forEach(
            function(block: GenericBlock) {
                block.setCellHighlight(cellCode, isOn);
            }
        );
        // Highlight the letter in the letter map.
        this.letterMap.setCellHighlight(cellCode, isOn);
    }
}
