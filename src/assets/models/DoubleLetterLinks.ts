export class DoubleLetterLinks {
    static readonly DIRECTION_UP = '_up';
    static readonly DIRECTION_DOWN = '_down';
    static readonly DIRECTION_LEFT = '_left';
    static readonly DIRECTION_RIGHT = '_right';

    private _up = false;
    private _down = false;
    private _left = false;
    private _right = false;

    public set(direction: string, newValue: boolean) {
        switch (direction) {
            case DoubleLetterLinks.DIRECTION_UP:
                this._up = newValue;
                break;
            case DoubleLetterLinks.DIRECTION_DOWN:
                this._down = newValue;
                break;
            case DoubleLetterLinks.DIRECTION_LEFT:
                this._left = newValue;
                break;
            case DoubleLetterLinks.DIRECTION_RIGHT:
                this._right = newValue;
                break;
        }
    }

    public hasUp(): boolean {
        return this._up;
    }

    public hasDown(): boolean {
        return this._down;
    }

    public hasLeft(): boolean {
        return this._left;
    }

    public hasRight(): boolean {
        return this._right;
    }
}
