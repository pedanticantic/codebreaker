import {GenericCell} from './genericCell';
import {GenericBlock} from '../blocks/genericBlock';
import {GameService} from '../../services/gameService';

export class StandardCell extends GenericCell {
    protected static isValidBlockCode(newValue: any): boolean {
        return (newValue === '?');
    }

    public setCodeNumber(newValue, parentBlock: GenericBlock, rowIdx: number, colIdx: number, gameService: GameService) {
        if (GenericCell.isValidGenericBlockCode(newValue) || StandardCell.isValidBlockCode(newValue)) {
            const oldValue = this.codeNumber;
            switch (newValue) {
                case '*':
                    newValue = 27;
                    break;
                case '?':
                    // If the cell is already a "?", leave it; otherwise generate the next index.
                    if (this.codeNumber >= 30) {
                        newValue = this.codeNumber;
                    } else {
                        newValue = gameService.getNextUnnumberedCellNumber();
                    }
                    break;
                default:
                    newValue = parseInt(newValue, 10);
                    break;
            }
            this.codeNumber = parseInt(newValue, 10); // It's important that it's a number, not a string.
            // If the old value is greater than 29 and new value is not, we need to re-organise the mapping
            // (splice them) and update all the cells in this block.
            if (oldValue >= 30 && newValue < 30) {
                gameService.removeUnnumberedCell(parentBlock, oldValue);
            }
            if (oldValue === 0) {
                if (newValue !== 0) {
                    // Tell the block to set any other appropriate cells to "unknown".
                    parentBlock.autoSetCode(rowIdx, colIdx, 27);
                }
            } else {
                if (newValue === 0) {
                    // Tell the block to set any other appropriate cells to 0.
                    // @TODO: Need to to handle unnumbered cells being set to 0.
                    parentBlock.autoSetCode(rowIdx, colIdx, 0);
                }
            }
        }
    }

    public getEditDisplayValue(): string {
        return null;
    }
}
