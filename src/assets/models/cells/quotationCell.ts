import {GenericCell} from './genericCell';
import {GenericBlock} from '../blocks/genericBlock';
import {GameService} from '../../services/gameService';

export class QuotationCell extends GenericCell {
    protected static isValidBlockCode(newValue: any): boolean {
        return (newValue === '.' || newValue === ',' || newValue === '?' || newValue === '\'');
    }

    public override isPrintableChar(): boolean {
        return super.isPrintableChar() || QuotationCell.isValidBlockCode(this.getRawValue());
    }

    public setCodeNumber(newValue, parentBlock: GenericBlock, rowIdx: number, colIdx: number, gameService: GameService) {
        if (GenericCell.isValidGenericBlockCode(newValue) || QuotationCell.isValidBlockCode(newValue)) {
            newValue = (newValue === '*' ? 27 : (parseInt(newValue, 10).toString() === newValue ? parseInt(newValue, 10) : newValue));
            this.codeNumber = newValue;
        }
    }

    public getEditDisplayValue(): string {
        // return the value, unless it's numeric, in which case return null.
        return (!isNaN(this.codeNumber) || parseInt(this.codeNumber, 10).toString() === this.codeNumber) ? null : this.codeNumber;
    }
}
