import {GenericBlock} from '../blocks/genericBlock';
import {GameService} from '../../services/gameService';
import {DoubleLetterLinks} from '../DoubleLetterLinks';

export abstract class GenericCell {
    protected codeNumber: any;
    protected highlightCell = false;
    protected doubleLetterLinks: DoubleLetterLinks;
    protected cellHasFocus = false;

    constructor() {
        this.codeNumber = 27;
        this.doubleLetterLinks = new DoubleLetterLinks();
    }

    protected static isValidGenericBlockCode(codeNumber: any): boolean {
        if (codeNumber === '*') {
            return true;
        }
        if (parseInt(codeNumber, 10).toString() === codeNumber) {
            if (codeNumber >= 0 && codeNumber <= 27) {
                return true;
            }
        }

        return false;
    }

    public abstract setCodeNumber(newValue, parentBlock: GenericBlock, rowIdx: number, colIdx: number, gameService: GameService);
    public abstract getEditDisplayValue(): string;

    public setRawValue(newValue) {
        this.codeNumber = newValue;
    }

    public getRawValue() {
        return this.codeNumber;
    }

    public getDisplayValue() {
        return this.codeNumber >= 30 ? '?' : this.codeNumber;
    }

    public getCellCode() {
        if (this.codeNumber === 27) {
            return '*';
        } else {
            if (this.codeNumber >= 30) {
                return '?';
            } else {
                return this.codeNumber;
            }
        }
    }

    public isPrintableChar(): boolean {
        return (this.codeNumber >= 1 && this.codeNumber <= 26) || this.codeNumber >= 30;
    }

    public isBlock(): boolean {
        return this.codeNumber === 0;
    }

    public isUnknown(): boolean {
        return this.codeNumber === 27;
    }

    public isUnnumbered(): boolean {
        return this.codeNumber >= 30;
    }

    public setHighlight(isOn: boolean) {
        this.highlightCell = isOn;
    }

    public isHighlighted(): boolean {
        return this.highlightCell;
    }

    public getDoubleLetterLinks(): DoubleLetterLinks {
        return this.doubleLetterLinks;
    }

    public setFocus(hasFocus: boolean) {
        this.cellHasFocus = hasFocus;
    }

    public getFocusClass() {
        return this.cellHasFocus ? 'focussed' : '';
    }
}
