import {GenericCell} from '../cells/genericCell';
import {DoubleLetterLinks} from '../DoubleLetterLinks';

export abstract class GenericBlock {
    protected rows: any[] = [];
    protected blockType: string;
    protected isSquareBlock = false;

    constructor(protected blockWidth: number, protected blockHeight: number, isSquare: boolean) {
        this.setBlockType();

        // Need need a blockWidth x blockHeight array of 27s (27 is the number after 26, obvs).
        this.buildBlock(blockWidth, blockHeight, isSquare);
    }

    protected abstract setBlockType();
    protected abstract makeNewCell();
    protected abstract checkForDoubles(direction: string): boolean;
    protected abstract propagateDoubleLetterLinksRefresh(rowIdx: number, colIdx: number);
    public abstract getBlockDescription(): string;
    public abstract autoSetCode(rowIdx: number, colIdx: number, newValue: any);
    public abstract addColumn();
    public abstract removeColumn();
    public abstract addRow();
    public abstract removeRow();
    public abstract isQuotation(): boolean;
    public abstract toggleSquareness();

    protected buildBlock(blockWidth: number, blockHeight: number, isSquare: boolean) {
        for (let rows = 0 ; rows < blockHeight ; rows++) {
            this.rows.push(this.buildEmptyRow(blockWidth));
        }
        this.setSize(blockWidth, blockHeight);
        // If width == height, it might not be fixed as square, so we need a separate flag.
        this.isSquareBlock = isSquare;
    }

    private setSize(xDim: number, yDim: number) {
        this.blockWidth = xDim;
        this.blockHeight = yDim;
    }

    public isSquare() {
        return this.isSquareBlock;
    }

    public widthEqualsHeight() {
        return this.blockWidth === this.blockHeight;
    }

    protected buildEmptyRow(numCells: number) {
        const emptyRow = [];
        for (let cols = 0 ; cols < numCells ; cols++) {
            emptyRow.push(this.makeNewCell());
        }

        return emptyRow;
    }

    public getType(): string {
        return this.blockType;
    }

    public setRows(rows: any[]) {
        let rowIdx = 0;
        for (const row of rows) {
            if (this.rows.hasOwnProperty(rowIdx)) {
                let cellIdx = 0;
                for (const cell of row) {
                    if (this.rows[rowIdx].hasOwnProperty(cellIdx)) {
                        // The cell is an object, of course, with a property of "codeNumber.
                        this.rows[rowIdx][cellIdx].setRawValue(cell.codeNumber);
                        // Update the information on double letters.
                        this.refreshDoubleLetterLinks(rowIdx, cellIdx, false);
                        cellIdx++;
                    }
                }
            }
            rowIdx++;
        }
    }

    public getRows() {
        return this.rows;
    }

    public shiftDownUnnumberedCellNumbers(from: number) {
        for (let rowIdx = 0 ; rowIdx < this.rows.length ; ++rowIdx) {
            const row = this.rows[rowIdx];
            for (let colIdx = 0 ; colIdx < row.length ; ++colIdx) {
                const cell: GenericCell = row[colIdx];
                if (cell.getRawValue() > from) {
                    cell.setRawValue(cell.getRawValue() - 1);
                }
            }
        }
    }

    public setCellHighlight(cellCode: number, isOn: boolean) {
        for (let rowIdx = 0 ; rowIdx < this.rows.length ; ++rowIdx) {
            const row = this.rows[rowIdx];
            for (let colIdx = 0 ; colIdx < row.length ; ++colIdx) {
                const cell: GenericCell = row[colIdx];
                if (cell.getCellCode() === cellCode) {
                    cell.setHighlight(isOn);
                }
            }
        }
    }

    protected refreshDoubleLetterLinksOneBlock() {
        const self = this;
        this.rows.forEach(
            function (row: GenericCell[], rowIdx: number) {
                row.forEach(
                    function (cell: GenericCell, colIdx: number) {
                        self.refreshDoubleLetterLinksOneCell(rowIdx, colIdx, true);
                    }
                );
            }
        );
    }

    /**
     * This method takes a cell, and refreshes the "double letter" links for that cell and its neighbours.
     * It also does the same for any cells in an "equivalent" position, eg if the board has rotational symmetry,
     * then the cell diagonally opposite this one.
     *
     * @param {number} rowIdx
     * @param {number} colIdx
     * @param {boolean} propagate
     */
    public refreshDoubleLetterLinks(rowIdx: number, colIdx: number, propagate: boolean) {
        this.refreshDoubleLetterLinksOneCell(rowIdx, colIdx, propagate);

        // Call the subclass method to run this process for "equivalent" cells.
        if (propagate) {
            this.propagateDoubleLetterLinksRefresh(rowIdx, colIdx);
        }
    }

    protected refreshDoubleLetterLinksOneCell(rowIdx: number, colIdx: number, checkDownAndRight: boolean) {
        const thisCell: GenericCell = this.rows[rowIdx][colIdx];

        // Determine whether the cell in each direction has the same code as this one, and update the links in
        // both cells accordingly. If this cell does not contain a printable character, it's automatically "no".
        if (rowIdx > 0 && this.checkForDoubles(DoubleLetterLinks.DIRECTION_UP)) {
            const newUp = this.rows[rowIdx - 1][colIdx].getRawValue() === thisCell.getRawValue() && thisCell.isPrintableChar();
            thisCell.getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_UP, newUp);
            this.rows[rowIdx - 1][colIdx].getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_DOWN, newUp);
        }
        if (colIdx > 0 && this.checkForDoubles(DoubleLetterLinks.DIRECTION_LEFT)) {
            const newLeft = this.rows[rowIdx][colIdx - 1].getRawValue() === thisCell.getRawValue() && thisCell.isPrintableChar();
            thisCell.getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_LEFT, newLeft);
            this.rows[rowIdx][colIdx - 1].getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_RIGHT, newLeft);
        }
        if (checkDownAndRight) {
            if (rowIdx < (this.rows.length - 1) && this.checkForDoubles(DoubleLetterLinks.DIRECTION_DOWN)) {
                const newDown = this.rows[rowIdx + 1][colIdx].getRawValue() === thisCell.getRawValue() && thisCell.isPrintableChar();
                thisCell.getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_DOWN, newDown);
                this.rows[rowIdx + 1][colIdx].getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_UP, newDown);
            }
            if (colIdx < (this.rows[rowIdx].length - 1) && this.checkForDoubles(DoubleLetterLinks.DIRECTION_RIGHT)) {
                const newRight = this.rows[rowIdx][colIdx + 1].getRawValue() === thisCell.getRawValue() && thisCell.isPrintableChar();
                thisCell.getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_RIGHT, newRight);
                this.rows[rowIdx][colIdx + 1].getDoubleLetterLinks().set(DoubleLetterLinks.DIRECTION_LEFT, newRight);
            }
        }
    }
}
