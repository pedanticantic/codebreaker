import {StandardBlock} from './standardBlock';

export class RotationalBlock extends StandardBlock {
    protected setBlockType() {
        this.blockType = 'rotational';
    }

    public getBlockDescription(): string {
        return this.blockWidth + ' x ' + this.blockHeight + ' ' +
            (this.isSquare() ? 'square' : 'non-square') + ' with rotational symmetry';
    }

    protected autoSetCodeAdditional(rowIdx: number, colIdx: number, oppositeRow: number, oppositeCol: number, newValue: any) {
        // There aren't any additional cells to set, so do nothing.
    }

    protected propagateDoubleLetterLinksRefresh(rowIdx: number, colIdx: number) {
        // We just refresh the cell that is diagonally opposite this one.
        this.refreshDoubleLetterLinksOneCell(this.rows.length - rowIdx - 1, this.rows[rowIdx].length - colIdx - 1, true);
    }
}
