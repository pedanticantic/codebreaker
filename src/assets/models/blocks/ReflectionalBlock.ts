import {StandardBlock} from './standardBlock';

export class ReflectionalBlock extends StandardBlock {
    protected setBlockType() {
        this.blockType = 'reflectional';
    }

    public getBlockDescription(): string {
        return this.blockWidth + ' x ' + this.blockHeight + ' ' +
            (this.isSquare() ? 'square' : 'non-square') + ' with full reflectional symmetry';
    }

    protected autoSetCodeAdditional(rowIdx: number, colIdx: number, oppositeRow: number, oppositeCol: number, newValue: any) {
        this.rows[oppositeRow][colIdx].setRawValue(newValue);
        this.rows[rowIdx][oppositeCol].setRawValue(newValue);
    }

    protected propagateDoubleLetterLinksRefresh(rowIdx: number, colIdx: number) {
        // We have to refresh the 3 cells that are opposite horizontally, vertically and diagonally.
        const oppColIdx = this.rows[rowIdx].length - colIdx - 1;
        const oppRowIdx = this.rows.length - rowIdx - 1;
        this.refreshDoubleLetterLinksOneCell(rowIdx, oppColIdx, true);
        this.refreshDoubleLetterLinksOneCell(oppRowIdx, colIdx, true);
        this.refreshDoubleLetterLinksOneCell(oppRowIdx, oppColIdx, true);
    }
}
