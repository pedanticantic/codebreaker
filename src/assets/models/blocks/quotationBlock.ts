import {GenericBlock} from './genericBlock';
import {QuotationCell} from '../cells/quotationCell';
import {DoubleLetterLinks} from '../DoubleLetterLinks';

export class QuotationBlock extends GenericBlock {
    constructor(protected override blockWidth: number, protected override blockHeight: number) {
        super(blockWidth, blockHeight, false);
    }

    protected makeNewCell() {
        return new QuotationCell();
    }

    protected checkForDoubles(direction: string): boolean {
        // We only check for doubles in the left-right direction.
        return direction === DoubleLetterLinks.DIRECTION_LEFT || direction === DoubleLetterLinks.DIRECTION_RIGHT;
    }

    protected propagateDoubleLetterLinksRefresh(rowIdx: number, colIdx: number) {
        return; // We do nothing for a quotation block.
    }

    public autoSetCode(rowIdx: number, colIdx: number, newValue: any) {
        // Do nothing, as it only makes sense for a standard block.
    }

    public isQuotation(): boolean {
        return true;
    }

    protected setBlockType() {
        this.blockType = 'quotation';
    }

    public getBlockDescription(): string {
        return this.blockWidth + 'W x ' + this.blockHeight + 'H quotation block';
    }

    public addColumn() {
        // Just add a quotation cell on to the end of each row and record the new width.
        for (const rowIdx in this.rows) {
            if (this.rows[rowIdx]) {
                this.rows[rowIdx].push(this.makeNewCell());
            }
        }
        ++this.blockWidth;

        this.refreshDoubleLetterLinksOneBlock();
    }

    public removeColumn() {
        // Remove the cell from the end of each row and record the new width.
        for (const rowIdx in this.rows) {
            if (this.rows[rowIdx]) {
                this.rows[rowIdx].pop();
            }
        }
        --this.blockWidth;

        this.refreshDoubleLetterLinksOneBlock();
    }

    public addRow() {
        // This is pretty easy - add an empty row to the bottom and update the block height.
        this.rows.push(this.buildEmptyRow(this.blockWidth));
        ++this.blockHeight;

        this.refreshDoubleLetterLinksOneBlock();
    }
    public removeRow() {
        // Again, this is pretty simple - pop the last row of the array and decrement the block height.
        this.rows.pop();
        --this.blockHeight;

        this.refreshDoubleLetterLinksOneBlock();
    }

    public toggleSquareness() {
        alert('You cannot toggle the squareness of a quotation block!');
    }
}
