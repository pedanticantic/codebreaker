/* This represents an instance of a "standard" codebreaker block - the thing that looks like a crossword puzzle. */
import {GenericBlock} from './genericBlock';
import {StandardCell} from '../cells/standardCell';

export abstract class StandardBlock extends GenericBlock {
    static readonly DEFAULT_BLOCK_SIZE = 15;

    protected abstract override setBlockType();

    protected abstract autoSetCodeAdditional(rowIdx: number, colIdx: number, oppositeRow: number, oppositeCol: number, newValue: any);

    public isQuotation(): boolean {
        return false;
    }

    protected makeNewCell() {
        return new StandardCell();
    }

    protected checkForDoubles(direction: string): boolean {
        return true; // We check for doubles in every direction.
    }

    public autoSetCode(rowIdx: number, colIdx: number, newValue: any) {
        const oppositeRow = this.blockHeight - rowIdx - 1;
        const oppositeCol = this.blockWidth - colIdx - 1;
        this.rows[oppositeRow][oppositeCol].setRawValue(newValue);
        this.autoSetCodeAdditional(rowIdx, colIdx, oppositeRow, oppositeCol, newValue);
    }

    public addColumn() {
        // We do different things depending on whether the block is square or not.
        if (this.isSquare()) {
            this.addColumnAndRow();
        } else {
            this.addJustColumn();
        }
        this.refreshDoubleLetterLinksOneBlock();
    }
    public removeColumn() {
        // We do different things depending on whether the block is square or not.
        if (this.isSquare()) {
            this.removeColumnAndRow();
        } else {
            this.removeJustColumn();
        }
        this.refreshDoubleLetterLinksOneBlock();
    }

    public addRow() {
        // User can only add/remove rows if the block is not square.
        if (this.isSquare()) {
            alert('You cannot add a row to a standard block - please use "add column"');
        } else {
            this.addJustRow();
            this.refreshDoubleLetterLinksOneBlock();
        }
    }

    public removeRow() {
        // User can only add/remove rows if the block is not square.
        if (this.isSquare()) {
            alert('You cannot remove a row from a standard block - please use "remove column"');
        } else {
            this.removeJustRow();
            this.refreshDoubleLetterLinksOneBlock();
        }
    }

    public toggleSquareness() {
        if (this.widthEqualsHeight()) {
            this.isSquareBlock = !this.isSquareBlock;
        } else {
            alert('Cannot toggle squareness - width and height are different');
        }
    }

    private addJustColumn() {
        // We need an empty column in the "middle" of the block.
        const newIndex = this.blockWidth % 2 === 0 ? (this.blockWidth / 2) : ((1 + this.blockWidth) / 2);
        for (const rowIdx in this.rows) {
            if (this.rows.hasOwnProperty(rowIdx)) {
                const newCell = this.makeNewCell();
                if (this.blockWidth % 2 === 0) {
                    // Even number of rows/columns.
                    // Cell can stay as "*".
                } else {
                    // Odd number of columns.
                    // If the corresponding cell in the preceding column is a black square, then our new one must be.
                    if (this.rows[this.blockHeight - parseInt(rowIdx, 10) - 1][newIndex - 1].getRawValue() === 0) {
                        newCell.setRawValue(0);
                    }
                }
                this.rows[rowIdx].splice(newIndex, 0, newCell);
            }
        }
        ++this.blockWidth;
    }

    private removeJustColumn() {
        // We're going to remove a column from more-or-less the centre of the block.
        // If there are an even number of columns in the block, we need to make sure the black cells are still
        // symmetrical, so we go through the column to the left of the column we're going to delete and force the
        // black blocks to be symmetrical.
        let removeIndex;
        if (this.blockWidth % 2 === 0) {
            // We're going to remove the column to the right of centre.
            removeIndex = this.blockWidth / 2;
            // Sort out the black blocks - make sure they will still be symmetrical after.
            for (let cellIdx = 0 ; cellIdx < this.blockHeight ; cellIdx++) {
                if (this.rows[cellIdx][removeIndex].getRawValue() === 0) {
                    this.rows[this.blockHeight - cellIdx - 1][removeIndex - 1].setRawValue(0);
                    this.rows[cellIdx][removeIndex - 1].setRawValue(0);
                }
            }
        } else {
            // The block has an odd number of columns: this is easy - just remove the middle column.
            removeIndex = (this.blockWidth - 1) / 2;
        }

        // Remove the column.
        for (const rowIdx in this.rows) {
            if (this.rows.hasOwnProperty(rowIdx)) {
                this.rows[parseInt(rowIdx, 10)].splice(removeIndex, 1);
            }
        }
        --this.blockWidth;
    }

    private addJustRow() {
        let newRow;
        let newIndex;
        if (this.blockWidth % 2 === 0) {
            // Even number of rows/columns.
            // This is easier - we can add an empty row and an empty column to the block.
            newRow = this.buildEmptyRow(this.blockWidth);
            newIndex = this.rows.length / 2;
        } else {
            // Odd number of rows/columns.
            // Take the row that is just before the centre, and build a new row the same, but with the cells in the
            // reverse order, and any actual numbers replaced with "*"s.
            newIndex = (this.rows.length + 1) / 2;
            const existingRow = this.rows[newIndex - 1];
            newRow = this.buildEmptyRow(this.blockWidth);
            for (const existingIdx in existingRow) {
                if (existingRow.hasOwnProperty(existingIdx)) {
                    const newValue = existingRow[existingIdx].getRawValue() === 0 ? 0 : 27;
                    newRow[this.blockWidth - parseInt(existingIdx, 10) - 1].setRawValue(newValue);
                }
            }
        }
        this.rows.splice(newIndex, 0, newRow);

        ++this.blockHeight;
    }

    private removeJustRow() {
        // We're going to remove a row from more-or-less the centre of the block.
        // If there are an even number of rows in the block, we need to make sure the black cells are still
        // symmetrical, so we go through the row above the row we're going to delete and force the black blocks
        // to be symmetrical.
        let removeIndex;
        if (this.blockHeight % 2 === 0) {
            // We're going to remove the row just below centre.
            removeIndex = this.blockHeight / 2;
            // Sort out the black blocks - make sure they will still be symmetrical after.
            for (let cellIdx = 0 ; cellIdx < this.blockWidth ; cellIdx++) {
                if (this.rows[removeIndex][cellIdx].getRawValue() === 0) {
                    this.rows[removeIndex - 1][cellIdx].setRawValue(0);
                    this.rows[removeIndex - 1][this.blockHeight - cellIdx - 1].setRawValue(0);
                }
            }
        } else {
            // The block has an odd number of rows: this is easy - just remove the middle row.
            removeIndex = (this.blockHeight - 1) / 2;
        }

        // Remove the row.
        this.rows.splice(removeIndex, 1);
        --this.blockHeight;
    }

    private addColumnAndRow() {
        // This is called when the block is square - when we add/remove a column, we must do the same with a row.
        // Add a row first.
        this.addJustRow();

        // Now add a column.
        this.addJustColumn();
    }

    private removeColumnAndRow() {
        // This is called when the block is square - when we add/remove a column, we must do the same with a row.
        // Remove a row.
        this.removeJustRow();

        // Then remove a column.
        this.removeJustColumn();
    }
}
