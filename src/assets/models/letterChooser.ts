import {GameService} from '../services/gameService';

export class LetterChooser {
    private letter: string;
    private certainty: number;
    private availableLetters: string[];
    private showAvailableLetters = false;

    constructor(
        private codeNumber: number,
        private gameService: GameService,
        private confirmCallback: Function
    ) {
        // We must build the list of available letters. It is all letters that haven't been selected, plus the
        // given letter, if passed in.
        this.availableLetters = this.gameService.getUnchosenLetters(codeNumber);
        this.letter = this.gameService.getLetterForNumber(codeNumber);
        this.certainty = this.gameService.getCertaintyForNumber(codeNumber, 25);
    }

    public getCode(): number {
        return this.codeNumber;
    }

    public getDisplayCode() {
        return this.codeNumber >= 30 ? '?' : this.codeNumber;
    }

    public getAvailableLetterGrid(): string[][] {
        const result = [];
        const rows = Math.ceil(this.availableLetters.length / 2);
        for (let rowIndex = 0 ; rowIndex < rows ; rowIndex++) {
            result[rowIndex] = [null, null];
            result[rowIndex][0] = this.availableLetters[rowIndex];
            if (rows + rowIndex < this.availableLetters.length) {
                result[rowIndex][1] = this.availableLetters[rows + rowIndex];
            }
        }

        return result;
    }

    public getCertainties(): number[] {
        // There is a special certainty, where the mapping is given, and we use a value of 101 for this.
        return [25, 50, 75, 100, 101];
    }

    public getSelectedLetter() {
        return this.letter;
    }

    public getSelectedCertainty() {
        return this.certainty;
    }

    public setLetter(newLetter: string) {
        this.letter = newLetter;
        this.hideShowAvailableLetters();
    }

    public setCertainty(certainty: number) {
        this.certainty = certainty;
    }

    public clearSelection() {
        // Pretend the user has cleared the letter droplist and clicked save.
        this.setLetter(null);
        this.confirmCallback();
    }

    public setShowAvailableLetters(isVisible: boolean) {
        this.showAvailableLetters = isVisible;
    }

    public toggleShowAvailableLetters() {
        this.setShowAvailableLetters(!this.showAvailableLetters);
    }

    public hideShowAvailableLetters() {
        this.setShowAvailableLetters(false);
    }

    public showingAvailableLetters(): boolean {
        return this.showAvailableLetters;
    }

    public handleKeyPress(ev: any) {
        // If they pressed BACKSPACE, just clear the letter.
        if (ev.keyCode === 8) {
            this.setLetter('');
        } else {
            const letterPressed = ev.key.toUpperCase();
            // See if the letter they pressed is available. If so, just pretend they selected it.
            if (this.availableLetters.indexOf(letterPressed) >= 0) {
                this.setLetter(letterPressed);
            }
        }
    }
}
