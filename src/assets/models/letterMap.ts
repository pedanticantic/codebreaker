import {Mapping} from './mapping';
import {LetterChooser} from './letterChooser';
import {GenericBlock} from './blocks/genericBlock';

export class LetterMap {
    private mapping: any[] = [];
    private maxUnnumberedCellNumber = 29;

    constructor() {
        // Initialise the array of mappings from number to letter. To start with, there are no mappings!
        for (let lIdx = 1 ; lIdx <= 26 ; lIdx++) {
            this.mapping[lIdx] = null;
        }
    }

    public setState(letterMapState) {
        letterMapState = letterMapState.mapping;
        for (const letterMapIndex in letterMapState) {
            if (letterMapState.hasOwnProperty(letterMapIndex)) {
                const thisMapState = letterMapState[letterMapIndex];
                this.mapping[letterMapIndex] =
                    thisMapState == null ? null : new Mapping(thisMapState.letter, thisMapState.certainty);
                if (parseInt(letterMapIndex, 10) >= 30) {
                    this.maxUnnumberedCellNumber = parseInt(letterMapIndex, 10);
                }
            }
        }
    }

    public getMapping(codeNumber: number) {
        return this.mapping[codeNumber];
    }

    public isLetterUsed(thisLetter: string, atAll = false): boolean {
        for (const mapIdx in this.mapping) {
            if (parseInt(mapIdx, 10) < 30 && this.mapping.hasOwnProperty(mapIdx)) {
                const mapped = this.mapping[mapIdx];
                if (mapped != null && mapped.getLetter() === thisLetter && (atAll || mapped.getCertainty() >= 100)) {
                    return true;
                }
            }
        }

        return false;
    }

    public getUnchosenLetters(codeNumber: number) {
        const allLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
        const unchosenLetters = [];
        unchosenLetters.push('');
        let thisLetter = '';
        const mapped = this.mapping[codeNumber];
        if (mapped != null) {
            thisLetter = mapped.getLetter();
        }
        for (const eachLetter of allLetters) {
            if (eachLetter === thisLetter || !this.isLetterUsed(eachLetter, true) || codeNumber >= 30) {
                unchosenLetters.push(eachLetter);
            }
        }

        return unchosenLetters;
    }

    public setMapping(chooser: LetterChooser) {
        let newMapping = null;
        if (chooser.getSelectedLetter() !== '') {
            newMapping = new Mapping(chooser.getSelectedLetter(), chooser.getSelectedCertainty());
        }

        this.mapping[chooser.getCode()] = newMapping;
    }

    public allAreCertain(): boolean {
        let numCertain = 0;
        for (const mapped of this.mapping) {
            if (mapped != null) {
                if (mapped.getCertainty() < 100) {
                    return false;
                }
                numCertain++;
            }
        }

        return numCertain >= 26;
    }

    public clearAllButGiven() {
        for (const mapIndex in this.mapping) {
            const thisMapping = this.mapping[mapIndex];
            if (thisMapping != null) {
                if (thisMapping.getCertainty() <= 100) {
                    this.mapping[mapIndex] = null;
                }
            }
        }
    }

    public getNextUnnumberedCellNumber(): number {
        // Increment the "next" available index and initialise the mapping at that position.
        ++this.maxUnnumberedCellNumber;
        this.mapping[this.maxUnnumberedCellNumber] = null;

        // Return the new next available index.
        return this.maxUnnumberedCellNumber;
    }

    public removeUnnumberedCell(block: GenericBlock, oldValue: number) {
        // The unnumbered cell with index <oldValue> is being removed.
        // Remove that entry from the mapping and shift all the ones after it up by 1.
        this.mapping.splice(oldValue, 1);
        // Then go through all the cells in the block, and decrement any that have a higher value.
        block.shiftDownUnnumberedCellNumbers(oldValue);
        // Decrement the next available number.
        --this.maxUnnumberedCellNumber;
    }

    public cycleCertainty(codeNumber: any): boolean {
        const mapping = this.mapping[codeNumber];
        if (mapping == null || mapping.certainty > 100) {
            return false;
        }

        // We are being a bit naughty and just assuming the allowed certainties.
        // @TODO: Make this use the array of certainties defined in the letter chooser. They will need to be moved
        // @TODO: out to another class.
        mapping.certainty += 25;
        if (mapping.certainty > 100) {
            mapping.certainty = 25;
        }

        return true;
    }

    public setCellHighlight(cellCode: number, isOn: boolean) {
        this.mapping.forEach(
            function(mapping: Mapping, codeNumber: number) {
                if (codeNumber === cellCode && mapping != null) {
                    mapping.setHighlight(isOn);
                }
            }
        );
    }

    public isCodeHighlighted(codeNumber: number): boolean {
        return this.mapping[codeNumber] != null && this.mapping[codeNumber].isHighlighted();
    }
}
