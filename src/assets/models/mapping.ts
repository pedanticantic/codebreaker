export class Mapping {
    constructor(
        private letter: string,
        private certainty: number,
        private highlighted = false
    ) {}

    public getLetter(): string {
        return this.letter;
    }

    public getCertainty(): number {
        return this.certainty;
    }

    public setHighlight(isOn: boolean) {
        this.highlighted = isOn;
    }

    public isHighlighted() {
        return this.highlighted;
    }
}
