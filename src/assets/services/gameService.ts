import {Injectable} from '@angular/core';
import {Game} from '../models/game';
import {LetterChooser} from '../models/letterChooser';
import {GenericBlock} from '../models/blocks/genericBlock';
import {GameRepository} from './gameRepository';
import {GenericCell} from '../models/cells/genericCell';

@Injectable()
export class GameService {
    private inProgress = false;
    private game: Game;

    constructor(private gameRepository: GameRepository) {}

    public isInProgress(): boolean {
        return this.inProgress;
    }

    public startGame() {
        // Determine the name. It's the lowest value of n for which there's no game with the name "New Game{{n}}"
        // Get all the games that have a name that starts with "New Game".
        const newGames = this.gameRepository.getGames().filter(
            game => game.getName().indexOf('New Game') !== -1
        );
        let gameIndex = 1;
        let found = false;
        let gameName = '';
        do {
            gameName = 'New Game' + gameIndex;
            const clashingGames = newGames.filter(
                game => game.getName() === gameName
            );
            found = clashingGames.length === 0;
            ++gameIndex;
        } while (!found);
        this.game = new Game(gameName);
        this.inProgress = true;
        this.gameRepository.save(this.game);
    }

    public isLetterUsed(thisLetter: string): boolean {
        return this.game.isLetterUsed(thisLetter);
    }

    public getBlocks(): GenericBlock[] {
        return this.game.blocks;
    }

    public getLetterForNumber(codeNumber: any): string {
        return this.game.getMappedLetter(codeNumber);
    }

    public getCertaintyForNumber(codeNumber: number, defaultValue: number = null): number {
        let result = this.game.getLetterCertainty(codeNumber);
        result = result > 0 ? result : defaultValue;

        return result;
    }

    public getUnchosenLetters(codeNumber: number): string[] {
        return this.game.letterMap.getUnchosenLetters(codeNumber);
    }

    public setMapping(chooser: LetterChooser) {
        this.game.setMapping(chooser);
        this.gameRepository.save(this.game);
    }

    public isCodeHighlighted(codeNumber: number): boolean {
        return this.game.isCodeHighlighted(codeNumber);
    }

    public gameIsComplete(): boolean {
        return this.game.isComplete();
    }

    public addReflectionalBlock(blockWidth: number, blockHeight: number, isSquare: boolean) {
        this.game.addReflectionalBlock(blockWidth, blockHeight, isSquare);
        this.gameRepository.save(this.game);
    }

    public addRotationalBlock(blockWidth: number, blockHeight: number, isSquare: boolean) {
        this.game.addRotationalBlock(blockWidth, blockHeight, isSquare);
        this.gameRepository.save(this.game);
    }

    public addQuotationBlock(blockWidth: number, blockHeight: number) {
        this.game.addQuotationBlock(blockWidth, blockHeight);
        this.gameRepository.save(this.game);
    }

    public deleteBlock(blockIdx: number) {
        this.game.deleteBlock(blockIdx);
        this.gameRepository.save(this.game);
    }

    public moveBlockBy(blockIdx: number, by: number) {
        this.game.moveBlockBy(blockIdx, by);
        this.gameRepository.save(this.game);
    }

    public addColumn(blockIdx: number) {
        this.game.addColumn(blockIdx);
        this.gameRepository.save(this.game);
    }

    public removeColumn(blockIdx: number) {
        this.game.removeColumn(blockIdx);
        this.gameRepository.save(this.game);
    }

    public addRow(blockIdx: number) {
        this.game.addRow(blockIdx);
        this.gameRepository.save(this.game);
    }

    public removeRow(blockIdx: number) {
        this.game.removeRow(blockIdx);
        this.gameRepository.save(this.game);
    }

    public toggleSquareness(blockIdx: number) {
        this.game.toggleSquareness(blockIdx);
        this.gameRepository.save(this.game);
    }

    public setCodeNumber(boardCell: GenericCell, newValue, parentBlock: GenericBlock, rowIdx: number, colIdx: number) {
        boardCell.setCodeNumber(newValue, parentBlock, rowIdx, colIdx, this);
        parentBlock.refreshDoubleLetterLinks(rowIdx, colIdx, true);
        this.gameRepository.save(this.game);
    }

    public loadGame(ref: number) {
        this.game = this.gameRepository.loadGame(ref);
        this.inProgress = true;
    }

    public deleteGame(): boolean {
        if (this.game.getRef()) {
            this.gameRepository.deleteGame(this.game.getRef());
            this.game = null;
            this.inProgress = false;

            return true;
        } else {
            return false;
        }
    }

    public getGameName(): string {
        return this.game.getName();
    }

    public setGameName(newName: string): boolean {
        // Check that it's not the same as any other game's name.
        // Find all the games, remove this one and ones that have a different name. Non-empty result means error.
        const clashingGames = this.gameRepository.getGames().filter(
            game => game.getRef() !== this.game.getRef() && game.getName() === newName
        );
        if (clashingGames.length) {
            alert('This name is already in use');

            return false;
        } else {
            this.game.setName(newName);
            this.gameRepository.save(this.game, true);

            return true;
        }
    }

    public resetGame() {
        this.game.resetGame();
        this.gameRepository.save(this.game, true);
    }

    public getNextUnnumberedCellNumber(): number {
        return this.game.letterMap.getNextUnnumberedCellNumber();
    }

    public removeUnnumberedCell(block: GenericBlock, oldValue: number) {
        this.game.letterMap.removeUnnumberedCell(block, oldValue);
    }

    public cycleCertainty(codeNumber: any) {
        if (this.game.letterMap.cycleCertainty(codeNumber)) {
            this.gameRepository.save(this.game, true);
        }
    }

    public setCellHighlightByBoardCell(blockCell: GenericCell, isOn: boolean) {
        // Check that the cell has a code.
        if (blockCell.isPrintableChar()) {
            // Go through all the cols in all the rows in all the blocks, and set the highlightByBoardCell accordingly if
            // its code matches this one.
            this.game.setCellHighlight(blockCell.getCellCode(), isOn);
        }
    }

    public setCellHighlightByCodeNumber(codeNumber: number, isOn: boolean) {
        this.game.setCellHighlight(codeNumber, isOn);
    }
}
