import {Inject, Injectable} from '@angular/core';
import {Game} from '../models/game';
import {LOCAL_STORAGE, WebStorageService} from 'ngx-webstorage-service';

@Injectable()
export class GameRepository {
    static readonly GAME_REFS_KEY = 'codebreaker-game-refs-key';
    static readonly GAME_REF_KEY_PREFIX = 'codebreaker-game-ref-';

    private gameNameRefs: number[]; // List of refs of all games that exist.
    private savedGames: Game[] = null;

    constructor(@Inject(LOCAL_STORAGE) private storage: WebStorageService) {
        // Make sure the element containing the list of games exists.
        this.gameNameRefs = this.storage.get(GameRepository.GAME_REFS_KEY);
        if (this.gameNameRefs === null || this.gameNameRefs === undefined) {
            this.gameNameRefs = [];
            this.saveKeys();
        }
    }

    private static buildGameKey(ref: number): string {
        return GameRepository.GAME_REF_KEY_PREFIX + ref;
    }

    private saveKeys() {
        this.storage.set(GameRepository.GAME_REFS_KEY, this.gameNameRefs);
    }

    public save(game: Game, forceReload?: boolean) {
        // If we don't know about this game, add it to the list of refs & save that.
        if (this.gameNameRefs.indexOf(game.getRef()) === -1) {
            this.gameNameRefs.push(game.getRef());
            this.saveKeys();
            this.forceReload();
        } else if (forceReload === true) {
            this.forceReload();
        }
        this.storage.set(GameRepository.buildGameKey(game.getRef()), game);
    }

    private forceReload() {
        this.savedGames = null; // Force a re-load of all the saved games.
    }

    private load(ref: number): Game {
        // @TODO: We should check that the ref exists in the list of refs, and that when we try to load it, we set something.
        return this.storage.get(GameRepository.buildGameKey(ref));
    }

    public getGames(): Game[] {
        if (this.savedGames == null) {
            this.savedGames = [];

            for (const gameRef of this.gameNameRefs) {
                const loadedGame = new Game();
                loadedGame.setState(this.load(gameRef));
                this.savedGames.push(loadedGame);
            }
            // Sort the results (by case-insensitive name). There should never be games with the same name, so don't bother checking.
            this.savedGames.sort(
                function(game1: Game, game2: Game) {
                    return game1.getName().toLocaleLowerCase() < game2.getName().toLocaleLowerCase() ? -1 : 1;
                }
            );
        }

        return this.savedGames;
    }

    public loadGame(ref: number): Game {
        if (this.gameNameRefs.indexOf(ref) === -1) {
            console.log('!!! Game does not exist !!!');
        }
        const games = this.getGames();
        const ourGames = games.filter(game => game.getRef() === ref);

        return ourGames.pop();
    }

    public deleteGame(ref: number) {
        // Basically remove the entry for the game itself, then remove the ref from the list of refs.
        this.storage.remove(GameRepository.buildGameKey(ref));
        this.gameNameRefs = this.gameNameRefs.filter(existingRef => existingRef !== ref);
        this.saveKeys();
        this.forceReload();
    }
}
