import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {GameComponent} from './game.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'game', component: GameComponent },
    { path: 'game/:ref', component: GameComponent },
];

export const routing = RouterModule.forRoot(appRoutes);
