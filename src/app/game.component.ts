import {Component} from '@angular/core';
import {GameService} from '../assets/services/gameService';
import {LetterChooser} from '../assets/models/letterChooser';
import {ActivatedRoute, Router, RouterLink} from '@angular/router';
import {GenericBlock} from '../assets/models/blocks/genericBlock';
import {StandardBlock} from '../assets/models/blocks/standardBlock';
import {GenericCell} from '../assets/models/cells/genericCell';
import {NgClass, NgForOf, NgIf} from '@angular/common';
import {FormsModule} from '@angular/forms';

@Component({
  templateUrl: 'game.component.html',
  styleUrls: ['game.css'],
  imports: [
    NgIf,
    FormsModule,
    NgClass,
    NgForOf,
    RouterLink
  ]
})
export class GameComponent {
    private availableLetters: any[] = [];
    private codeNumbers: any[] = [];
    public chooser: LetterChooser = null;
    private MODE_EDIT = 'EDIT';
    private MODE_PLAY = 'PLAY';
    private playMode = this.MODE_EDIT;
    public isSquare = true;
    public newBlockWidth = StandardBlock.DEFAULT_BLOCK_SIZE;
    public newBlockHeight = StandardBlock.DEFAULT_BLOCK_SIZE;
    public newQuotationWidth = 25;
    public newQuotationHeight = 4;
    private nameIsBeingEdited = false;
    public newGameName: string;

    constructor(
        private gameService: GameService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.route.params.subscribe(params => {
            if (params['ref']) {
                this.gameService.loadGame(parseInt(params['ref'], 10));
                this.playMode = this.MODE_PLAY;
            }
        });
        for (let availRow = 0 ; availRow < 13 ; availRow++) {
            this.availableLetters[availRow] = [];
            const firstCol = 65 + availRow;
            this.availableLetters[availRow].push(String.fromCharCode(firstCol));
            this.availableLetters[availRow].push(String.fromCharCode(13 + firstCol));

            this.codeNumbers.push([1 + availRow, 14 + availRow]);
        }
        if (!this.gameService.isInProgress()) {
            this.router.navigate(['/']);
        }
    }

    public certaintyToDisplay(certainty: number) {
        return certainty <= 100 ? (certainty + '%') : 'Given';
    }

    public gameInProgress(): boolean {
        return this.gameService.isInProgress();
    }

    public getAvailableLetters() {
        return this.availableLetters;
    }

    public getCodeNumbers() {
        return this.codeNumbers;
    }

    public isUsed(thisLetter: string): boolean {
        return this.gameService.isLetterUsed(thisLetter);
    }

    public getBlocks(): GenericBlock[] {
        return this.gameService.getBlocks();
    }

    public getLetter(codeNumber: any) {
        return this.gameService.getLetterForNumber(codeNumber);
    }

    public getCertainty(codeNumber: number): number {
      return this.gameService.getCertaintyForNumber(codeNumber);
    }

    public getHighlightClass(codeNumber: number): string {
        return this.gameService.isCodeHighlighted(codeNumber) ? 'highlighted' : '';
    }

    public showLetterChooser(codeNumber: number) {
        if (this.inPlayMode()) {
            if ((codeNumber >= 1 && codeNumber <= 26) || codeNumber >= 30) {
                const self = this;
                this.chooser = new LetterChooser(codeNumber, this.gameService, function () {
                    self.confirmLetter();
                });
            }
        }
    }

    public closeLetterChooser() {
        this.chooser = null;
    }

    public confirmLetter() {
        this.gameService.setMapping(this.chooser);
        this.closeLetterChooser();
    }

    public gameIsComplete(): boolean {
        return this.gameService.gameIsComplete();
    }

    public inEditMode(): boolean {
        return this.playMode === this.MODE_EDIT;
    }

    public inPlayMode(): boolean {
        return this.playMode === this.MODE_PLAY;
    }

    public toggleMode() {
        // Cancel any letter chooser pop-up.
        this.closeLetterChooser();
        // Toggle the mode.
        this.playMode = this.playMode === this.MODE_PLAY ? this.MODE_EDIT : this.MODE_PLAY;
    }

    private forceIntegers() {
        // It's very weird in that the block width can sometimes be a string, so we have to convert it to an
        // integer. We do the same for height, too, just in case.
        this.newBlockWidth = parseInt(this.newBlockWidth.toString(), 10);
        this.newBlockHeight = parseInt(this.newBlockHeight.toString(), 10);
    }

    public addReflectional() {
        this.forceIntegers();
        this.gameService.addReflectionalBlock(this.newBlockWidth, this.newBlockHeight, this.isSquare);
    }

    public addRotational() {
        this.forceIntegers();
        this.gameService.addRotationalBlock(this.newBlockWidth, this.newBlockHeight, this.isSquare);
    }

    public addQuotation() {
        this.gameService.addQuotationBlock(this.newQuotationWidth, this.newQuotationHeight);
    }

    public deleteBlock(blockIdx: number) {
        if (confirm('Are you sure you want to delete this block?')) {
            this.gameService.deleteBlock(blockIdx);
        }
    }

    public moveBlockDown(blockIdx: number) {
        this.gameService.moveBlockBy(blockIdx, 1);
    }

    public moveBlockUp(blockIdx: number) {
        this.gameService.moveBlockBy(blockIdx, -1);
    }

    public addColumn(blockIdx: number) {
        this.gameService.addColumn(blockIdx);
    }

    public removeColumn(blockIdx: number) {
        if (confirm('Are you sure you want to remove a column from this block?')) {
            this.gameService.removeColumn(blockIdx);
        }
    }

    public addRow(blockIdx: number) {
        this.gameService.addRow(blockIdx);
    }

    public removeRow(blockIdx: number) {
        if (confirm('Are you sure you want to remove a column from this block?')) {
            this.gameService.removeRow(blockIdx);
        }
    }

    public toggleSquareness(blockIdx: number) {
        if (confirm('Are you sure you want to toggle the squareness this block?')) {
            this.gameService.toggleSquareness(blockIdx);
        }
    }

    public setBoardCol(event, boardCell: GenericCell, parentBlock: GenericBlock, rowIdx: number, colIdx: number) {
        this.gameService.setCodeNumber(boardCell, event.target.value, parentBlock, rowIdx, colIdx);
    }

    public getCellCode(codeNumber) {
        return codeNumber === 27 ? '*' : codeNumber;
    }

    public deleteGame() {
        if (confirm('Are you sure you want to delete the game?')) {
            if (this.gameService.deleteGame()) {
                this.router.navigate(['/home']);
            }
        }
    }

    public getGameName() {
        return this.gameService.getGameName();
    }

    public editingName(): boolean {
        return this.nameIsBeingEdited;
    }

    public editName() {
        this.nameIsBeingEdited = true;
        this.newGameName = this.gameService.getGameName();
    }

    public cancelEditName() {
        this.nameIsBeingEdited = false;
    }

    public saveName() {
        if (this.gameService.setGameName(this.newGameName)) {
            this.nameIsBeingEdited = false;
        }
    }

    public resetGame() {
        if (confirm('Are you sure you want to clear all the letters except the ones that were given?')) {
            this.gameService.resetGame();
        }
    }

    public cycleCertainty(codeNumber: any) {
        // The double-click causes: the letter chooser to appear and the text to be selected. Reverse both of these.
        this.closeLetterChooser();
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }

        this.gameService.cycleCertainty(codeNumber);
    }

    public highlightByBoardCell(blockCell: GenericCell, isOn: boolean) {
        this.gameService.setCellHighlightByBoardCell(blockCell, isOn);
    }

    public highlightByCodeNumber(codeNumber: number, isOn: boolean) {
        this.gameService.setCellHighlightByCodeNumber(codeNumber, isOn);
    }

    public toggleSquare() {
        this.isSquare = !this.isSquare;
        if (this.isSquare) {
            // It is now square, so "width" has to follow "height". Pretend the user has just changed the width.
            this.blockWidthHasChanged();
        }
    }

    public blockWidthHasChanged() {
        // If the user is saying they want a square block, then block height must follow block width when the
        // latter is changed.
        if (this.isSquare) {
            this.newBlockHeight = this.newBlockWidth;
        }
    }
}
