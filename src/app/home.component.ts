import {Component} from '@angular/core';
import {GameService} from '../assets/services/gameService';
import {Router, RouterLink} from '@angular/router';
import {GameRepository} from '../assets/services/gameRepository';
import {Game} from '../assets/models/game';
import {NgForOf, NgIf} from '@angular/common';

@Component({
  templateUrl: 'home.component.html',
  imports: [
    NgIf,
    NgForOf,
    RouterLink
  ]
})
export class HomeComponent {
    constructor(
        private router: Router,
        private gameService: GameService,
        private gameRepository: GameRepository
    ) {}

    public newGame() {
        this.gameService.startGame();
        this.router.navigate(['/game']);
    }

    public getGamesList(): Game[] {
        return this.gameRepository.getGames();
    }
}
