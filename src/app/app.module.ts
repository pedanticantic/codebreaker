import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {GameService} from '../assets/services/gameService';
import {routing} from './app.routing';
import {HomeComponent} from './home.component';
import {GameComponent} from './game.component';
import {FormsModule} from '@angular/forms';
import {GameRepository} from '../assets/services/gameRepository';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import {StorageServiceModule} from 'ngx-webstorage-service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    AppComponent,
    HomeComponent,
    GameComponent,
    NgbModule,
    // StorageServiceModule,
  ],
    providers: [
        GameService,
        GameRepository,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
